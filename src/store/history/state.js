export default function() {
    return {
        history: [{
                "id": "1",
                "total": "1",
                "completed": true
            },
            {
                "id": "2",
                "total": "2",
                "completed": true
            },
            {
                "id": "3",
                "total": "100",
                "completed": false
            },
            {
                "id": "4",
                "total": "4",
                "completed": true
            }
        ],
        historyDetails: {},
    }
}