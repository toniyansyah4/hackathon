const setHistory = (state, history) => {
    state.history = history
}

const addHistory = (state, payload) => {
    console.log('payload: ', payload)

    Vue.set(state.history, payload.id, payload.title, payload.complete)
}

export { setHistory, addHistory }